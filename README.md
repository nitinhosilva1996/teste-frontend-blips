# Desafio frontend da <a href="https://blips.com.br/">Blips</a>

## Aplicação em React, utilizando uma API pública para listar (nome, capital, bandeira e línguas faladas) de cada país.

<hr />

# Contents

- [Screenshots](#screenshots)
- [Technologies](#technologies)
- [Getting started](#getting-started)
- [Running tests](#running-tests)
- [Contributing](#issues)
- [License](#license)

# Screenshots

<div align="center">
   <img src="./Screenshots/fullscreen.png" >
   <img src="./Screenshots/mobile.png" >
</div>

- [Clique aqui para acessar o site!](https://blips-challenge.web.app/)

# Technologies

This project was made using the follow technologies:

- [React](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/pt-BR/)
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/)
- [ESLint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [EditorConfig](https://editorconfig.org/)
- [Ant Design](https://ant.design/)
- [Styled Components](https://styled-components.com/)
- [Firebase Hosting](https://firebase.google.com/docs/hosting/)
- [API](https://restcountries.eu/)

# Getting started

```bash
# Clone Repository
$ git clone https://gitlab.com/nitinhosilva1996/teste-frontend-blips.git && cd ig-news

# Install Dependencies
$ yarn

# Fill .env.local file with YOUR environment variables, according to .env.example file.

# Run Aplication
$ yarn start
```

Go to http://localhost:3000/ to see the application running.

# Running tests

```bash
$ yarn test

# Run with --coverage to generate a new coverage report
$ yarn test --coverage
```

# Contributing

- Fork this repository;
- Create a new branch to develop your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'feat: my new feature'`;
- Push to your branch: `git push origin my-feature`.

# License

This project is under the [MIT License](./LICENSE) |
Made with 💖 by [Ivanilton Bezerra](https://www.linkedin.com/in/ivanilton-bezerra-da-silva-b67784108/).
