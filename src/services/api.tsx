export const API_URL = 'https://restcountries.eu/rest/v2';

interface Api {
  url: any;
  options: any;
}

export function COUNTRIES(): Api {
  return {
    url: `${API_URL}/all`,
    options: {
      method: 'GET',
      cache: 'no-store',
    },
  };
}
