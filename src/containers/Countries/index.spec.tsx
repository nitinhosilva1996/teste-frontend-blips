import { render } from '@testing-library/react';
import Countries from './index';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector() {
    return {
      loading: false,
      data: null,
      error: null,
    };
  },
  useDispatch: () => mockDispatch,
}));

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener() {},
      removeListener() {},
    };
  };

describe('Countries page', () => {
  it('render countries correctly', () => {
    render(<Countries />);
  });
});
