import { useEffect, useState } from 'react';
import { Spin } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { requestCountries } from '../../store/country.jsx';

import CountriesList from './components/CountriesList/CountriesList';
import { RootState } from '../../store/configureStore.js';

interface ILanguage {
  name: string;
}

function Countries(): JSX.Element {
  const dispatch = useDispatch();
  const { loading, data } = useSelector((state: RootState) => state.country);

  const [formattedData, setFormattedData] = useState<any>(null);

  function formatData(countries: Array<any>) {
    const result = countries.map(item => ({
      name: item.translations.pt,
      capital: item.capital,
      flag: item.flag,
      languages: item.languages.map(({ name }: ILanguage): string => name),
    }));

    setFormattedData(result);
  }

  useEffect(() => {
    if (!data) return;
    formatData(data || []);
  }, [data]);

  useEffect(() => {
    dispatch(requestCountries());
  }, []);

  return (
    <Spin tip="Carregando..." spinning={loading}>
      <CountriesList data={formattedData || []} />
    </Spin>
  );
}

export default Countries;
