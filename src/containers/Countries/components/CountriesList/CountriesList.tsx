import { List } from 'antd';
import CountriesListItem from '../CountriesListItem/CountriesListItem';

interface ICountry {
  name: string;
  capital: string;
  flag: string;
  languages: string[];
}

interface IList {
  data: Array<ICountry>;
}

function CountriesList({ data }: IList): JSX.Element {
  const grid = {
    gutter: 16,
    xs: 1,
    sm: 2,
    md: 3,
    lg: 4,
    xl: 4,
    xxl: 4,
  };
  return (
    <List
      grid={grid}
      dataSource={data}
      pagination={{ position: 'bottom', total: data.length }}
      renderItem={item => (
        <List.Item>
          <CountriesListItem {...item} />
        </List.Item>
      )}
    />
  );
}

export default CountriesList;
