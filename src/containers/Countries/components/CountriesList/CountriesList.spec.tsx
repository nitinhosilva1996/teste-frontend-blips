import { render, screen } from '@testing-library/react';
import CountriesList from './CountriesList';

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };

describe('CountriesList component', () => {
  it('render countries correctly', () => {
    const countries = [
      {
        name: 'Brasil',
        capital: 'Brasília',
        flag: 'https://restcountries.eu/data/bra.svg',
        languages: ['Português'],
      },
    ];

    render(<CountriesList data={countries} />);

    //screen.logTestingPlaygroundURL();
    const name = screen.getByText(/Brasil/i);
    const flag = screen.getByRole('img', {
      name: /bandeira/i,
    });

    expect(name).toBeInTheDocument();
    expect(flag).toBeInTheDocument();
  });
});
