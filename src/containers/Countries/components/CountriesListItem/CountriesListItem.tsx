import { Card, Tag, Divider } from 'antd';

import { Title } from './styles';

interface ICountry {
  name: string;
  capital: string;
  flag: string;
  languages: string[];
}

const CountriesListItem = (props: ICountry): JSX.Element => {
  const { name, flag, capital, languages } = props;

  function renderTitle(): JSX.Element {
    return (
      <Title>
        <span>{name}</span>
        <img src={flag} alt="bandeira" />
      </Title>
    );
  }

  return (
    <Card title={renderTitle()}>
      <span>Capital: {capital}</span>
      <Divider>Línguas faladas</Divider>
      {languages.map(item => (
        <Tag key={item} color="processing">
          {item}
        </Tag>
      ))}
    </Card>
  );
};

export default CountriesListItem;
