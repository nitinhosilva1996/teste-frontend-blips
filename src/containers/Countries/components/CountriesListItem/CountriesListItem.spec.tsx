import { render, screen } from '@testing-library/react';
import CountriesListItem from './CountriesListItem';

describe('CountriesListItem component', () => {
  it('render country correctly', () => {
    const country = {
      name: 'Brasil',
      capital: 'Brasília',
      flag: 'https://restcountries.eu/data/bra.svg',
      languages: ['Português'],
    };

    render(<CountriesListItem {...country} />);

    const name = screen.getByText(/Brasil/i);
    const flag = screen.getByRole('img', {
      name: /bandeira/i,
    });
    const capital = screen.getByText(/Brasília/i);
    const language = screen.getByText(/Português/i);

    expect(name).toBeInTheDocument();
    expect(flag).toBeInTheDocument();
    expect(capital).toBeInTheDocument();
    expect(language).toBeInTheDocument();
  });
});
