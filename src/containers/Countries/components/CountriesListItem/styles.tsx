import styles from 'styled-components';

export const Title = styles.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  img {
    height: 1.5rem;
    width: 2rem;
  }
`;
