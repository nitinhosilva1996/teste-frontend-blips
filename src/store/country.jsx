import { COUNTRIES } from '../services/api';
import createAsyncSlice from './helper/createAsyncSlice.jsx';

const slice = createAsyncSlice({
  name: 'country',
  fetchConfig: () => COUNTRIES(),
});

export const fetchCountries = slice.asyncAction;
export const { addCountries } = slice.actions;

export const requestCountries = () => async dispatch => {
  await dispatch(fetchCountries());
};

export default slice.reducer;
