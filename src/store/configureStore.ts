import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';

import country from './country.jsx';

const middleware = [...getDefaultMiddleware()];
const reducer = combineReducers({ country });
const store = configureStore({ reducer, middleware });

export type RootState = ReturnType<typeof reducer>;

export default store;
