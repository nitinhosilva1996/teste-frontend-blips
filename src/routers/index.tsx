import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Countries from '../containers/Countries/index';
import { Container } from './styles';

export default function App(): JSX.Element {
  return (
    <Container>
      <Router>
        <Switch>
          <Route path="/">
            <Countries />
          </Route>
        </Switch>
      </Router>
    </Container>
  );
}
