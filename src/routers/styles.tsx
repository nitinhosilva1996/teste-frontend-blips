import styles from 'styled-components';

export const Container = styles.div`
  height: 100%;
  width: 100%;
  overflow: hidden;
  padding: 4rem 1.5rem;
  margin: 0 auto;
  max-width: 75rem;
`;
